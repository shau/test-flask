const webpack = require('webpack');
const resolve = require('path').resolve;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
    mode: 'production',
    devtool: 'eval-source-map',
    entry: __dirname + '/js/main.js',
    output:{
        path: resolve('../public'),
        filename: 'bundle.js',
        publicPath: resolve('../public')
    },
    resolve: {
        extensions: ['.js','.css']
    },
    optimization: {
        minimizer: [
            new UglifyJsPlugin({
                extractComments: true,
                uglifyOptions: {
                    warnings: false,
                    parse: {},
                    compress: {},
                    mangle: true, // Note `mangle.properties` is `false` by default.
                    output: null,
                    toplevel: false,
                    nameCache: null,
                    ie8: false,
                    keep_fnames: false,
                  }
            })
        ]
    }
};
module.exports = config;