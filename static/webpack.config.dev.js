const webpack = require('webpack');
const resolve = require('path').resolve;
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const config = {
    mode: 'development',
    devtool: 'eval-source-map',
    entry: __dirname + '/js/main.js',
    output:{
        path: resolve('../public'),
        filename: 'bundle.js',
        publicPath: resolve('../public')
    },
    resolve: {
        extensions: ['.js','.css']
    },
};
module.exports = config;